import Levenshtein

# Beispiel für einfachen Vergleich
distance = Levenshtein.distance("Nutella 100g", "Nutella 50g")
print(distance)  # Gibt die Levenshtein-Distanz aus

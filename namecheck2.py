from rapidfuzz import fuzz, process

# Beispiel für einfachen Vergleich
ratio = fuzz.ratio("Nutella 100g", "Nutella 50g")
print(ratio)  # Gibt eine Ähnlichkeitsbewertung aus

# Beispiel für die Suche nach dem besten Match in einer Liste
choices = ["Nutella 100g", "Nutella 50g", "Nutella 200g"]
best_match = process.extractOne("Nutella 300g", choices)
print(best_match)  # Gibt das beste Match und seine Bewertung aus

import tensorflow as tf
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
import numpy as np
import matplotlib.pyplot as plt

# Funktion zum Extrahieren von Features
def extract_features(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = preprocess_input(img_data)
    features = model.predict(img_data)
    return features

# Lade das vortrainierte MobileNetV2-Modell
model = MobileNetV2(weights='imagenet', include_top=False, pooling='avg')

# Pfade zu Beispielbildern
image_path_1 = 'schuhe.jpg'
image_path_2 = 'schuhe2.jpg'

# Extrahiere Features für beide Bilder
features_image1 = extract_features(image_path_1)
features_image2 = extract_features(image_path_2)

# Berechne die Kosinusähnlichkeit zwischen den beiden Feature-Vektoren
similarity = np.dot(features_image1, features_image2.T) / (np.linalg.norm(features_image1) * np.linalg.norm(features_image2))

# Visualisiere die Bilder
img1 = image.load_img(image_path_1, target_size=(224, 224))
img2 = image.load_img(image_path_2, target_size=(224, 224))

plt.figure(figsize=(10, 4))
plt.subplot(1, 2, 1)
plt.imshow(img1)
plt.title("Image 1: Shoe")

plt.subplot(1, 2, 2)
plt.imshow(img2)
plt.title("Image 2: Shoe 1")
plt.show()

similarity_score = similarity[0][0]
print(f'Kosinusähnlichkeit: {similarity_score}')

# Bestimme, ob die Bilder ähnlich sind oder nicht
threshold = 0.5  # Du kannst diesen Schwellenwert anpassen
if similarity_score > threshold:
    print("Die Bilder sind ähnlich.")
else:
    print("Die Bilder sind nicht ähnlich.")

from difflib import SequenceMatcher

# Beispiel für einfachen Vergleich
ratio = SequenceMatcher(None, "Nutella 150g", "Nutella 100g", "Nutella 50g").ratio()
print(ratio)  # Gibt eine Ähnlichkeitsbewertung aus
